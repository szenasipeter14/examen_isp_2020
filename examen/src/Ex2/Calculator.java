package Ex2;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Calculator extends JFrame {



    JLabel operand1,operand2;
    JTextField op1,op2,rezultat;
    JButton calc;

    Calculator() {
        setTitle("Calculator");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500, 500);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);
        int width=80;int height = 20;

        operand1 = new JLabel("Operand 1 ");
        operand1.setBounds(10, 50, width, height);

        operand2 = new JLabel("Operand 2 ");
        operand2.setBounds(10, 100,200, height);




        op1 = new JTextField();
        op1.setBounds(150,50,width, height);

        op2 = new JTextField();
        op2.setBounds(150,100,width, height);



        rezultat = new JTextField();
        rezultat.setBounds(150,200,width, height);

        calc = new JButton("Calculeaza");
        calc.setBounds(10,200,100, height);
        calc.addActionListener(new TratareButon());

        add(operand1);add(operand2);add(op1);add(op2);add(rezultat);add(calc);
    }



    public static void main(String[] args) {

        new Calculator();
    }

    class TratareButon implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {

            String o1= Calculator.this.op1.getText();
            String o2= Calculator.this.op2.getText();
            int rez=0;
            rez = Integer.parseInt(o1) * Integer.parseInt(o2);
            Calculator.this.rezultat.setText(String.valueOf(rez));


           // System.out.println(rez);


        }
    }
}
